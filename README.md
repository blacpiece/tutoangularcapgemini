# Angular tuto

test

[[_TOC_]]

## Pré requis

- vscode :
  - [https://code.visualstudio.com/](vscode)
- node :
  - Télécharger et installer la dernière [+ version LTS +] de Node.js ici : [https://nodejs.org/en/download/]
- npm :
  - commande : `npm install -g npm@latest`
- angular CLI :
  - commande : `npm install -g @angular/cli`

- Modification de la version de node :
  - utilisation de [+ nvm +] qui permet d'avoir plusieurs versions de node et de switch entre les versions.
  - Télécharger nvm-setup.zip depuis [https://github.com/coreybutler/nvm-windows/releases] et exécuter nvm-setup.exe
  - `nvm version` permet de voir la version de nvm installé
  - `nvm install v10.15.0` permet d'installer une version de node spécifique
  - `nvm use 10.15.0` permet d'utiliser la version de node souhaité

### Vérification

- test vscode :
  - commande : `code ./` permet d'ouvrir dans vscode le répertoire dans lequel vous vous trouvez

- test node :
  - commande : `node --version` permet de connaitre la version de node installée

- test npm :
  - commande : `npm --version`permet de connaitre la version de npm installée

- test angular CLI :
  - commande : `ng version` permet de connaitre la version d'angular CLI

## Extensions vscode

- Angular essentials de John papa
- icon theme : material icon theme

## Parametres vscode

- auto save :
  - onFocusChange (en fonction de vos préférences)
- décocher :
  - compact folder
- cocher :
  - `Auto Guess Encoding` (permet de modifier l'encodage d'ouverture des fichiers en fonction de l'encodage de base du fichier)

## Création du projet

1 ng new
2 nom du projet
3 routing ? oui       --> génère automatiquement les fichiers de routing
4 styleshett ? scss   --> plus d'outils

### Bootstrap

- Installation :
  - commande : `npm install bootstrap`
  - [https://getbootstrap.com/docs/5.1/getting-started/introduction/](bootstrapv5.1)
  - [https://bootswatch.com/default/](bootswatch)
  - import dans `styles.scss` : 
```scss
@import '~bootstrap/scss/bootstrap';
```

### NgBootstrap

- Installation :
  - commande : `npm i @ng-bootstrap/ng-bootstrap`
  - [https://ng-bootstrap.github.io/#/home](ngbootstrap)
  - composant bootstrap fait spécifiquement pour angular
  - import dans les modules ou il est utilisé : `NgbModule`

### GIT ?

Git global setup
>>>
`git config --global user.name "myName"`

`git config --global user.email "myEmail"`
>>>

Create a new repository
>>>
`git clone <lien git>`

`cd <nom de votre projet>`

`git switch -c main`

`touch README.md`

`git add README.md`

`git commit -m "add README"`

`git push -u origin main`
>>>

Push an existing folder
>>>
`cd existing_folder`

`git init --initial-branch=main`

`git remote add origin <lien git>`

`git add .`

`git commit -m "Initial commit"`

`git push -u origin main`
>>>

Push an existing Git repository
>>>
`cd existing_repo`

`git remote rename origin old-origin`

`git remote add origin <lien git>`

`git push -u origin --all`

`git push -u origin --tags`
>>>
## Les différents fichiers / dossiers

- fichiers racines :
  - tsconfig.json
    - configuration du typescript
  - package.json
    - id du projet
    - script, dependencies, devDependencies
  - jasmine et karma
    - tests unitaires
  - angular.json
    - carte id du projet
    - configuration de build (prod / dev)
    - budget (vérification de la taille de votre application lors de la mise en production)
  - editorconf
    - règles de développement de vscode (taille des lignes, les quote_type, etc ...)
  - browserlist
    - liste des navigateurs pris en compte par votre version d'angular
[]
- dossiers racines :
  - src
    - contenu de votre applicaiton
  - node_modules
    - tous les fichiers nodes (ne pas y toucher et [- ne pas push sur un git ! -])
[]
- fichier src :
  - test.ts
    - fichier de lancement des tests unitaires
  - styles.scss
    - vos stylesheets globales
    - [+ import de bootstrap : `@import '~bootstrap/scss/bootstrap';` +]
  - polyfills.ts
    - retrocompatibilité de votre application avec d'anciennes version de votre navigateur
  - main.ts
    - premier fichier compilé, l'entrée d'angular
  - index.html
    - la base de votre html

### App

- composition :
  - app.module.ts
    - la carte de votre projet (déclaration des modules utilisés dans votre projet)
  - app.component.ts
    - votre composant root, tout par de ce composant, typescript comme html

## Structure d'un projet angular

>>>
[+ Bonne pratique +]
Les modules ou composants pour le nomage ne prennent pas de majuscules.
[+ Pour la structure d'un projet angular, il faut s'imaginer que ça fonctionne comme pour une maison : +]
Chaque pièces de la maison représente un module, chaque modules va avoir sa fonctionnalité : le salon, la cuisine, la chambre ....
Dans chaque pièces, il va y avoir des objects qui vont eux aussi avoir une fonctionnalité précise, ce sont des composant : dans la chambre il y a un lit, des tables de chevets, une armoire, etc ...
>>>

- Le but pour vous est de concevoir la meilleur maison avec la meilleur optimisation :
  - combien de chambre vous avez besoin (les modules),
  - quels sont les objects qui compose la cuisine de la maison (les composants),
  - quelles pièces ont besoins d'électricité (les services),
  - quels objects seront réutilisé dans les différentes pièces de votre maison (les composants partagés),
  - quelles pièces sont les plus utiles (les stratégies de chargements),
  - etc ...

### Modules obligatoires

- ui :
  - User interface
  - footer / header / navbar / etc ...
- shared
  - components / directives / enums / interfaces réutilisé dans votre application
  - bandeau / button / select / tableau
- core
  - guards / services utilisé pour toute l'application
- page-not-found
  - permet de gérer les routes non utilisées

### Création des différents composants d'angular

Commande de création : `ng g <mon composant>`
Ou depuis vscode schematics : click droit sur le fichier --> schematics --> angular --> mon composant voulut

- component
- module
- enum
- service
- guard
- directive
- interceptor

### Les modules

- composition :
  - `<le nom de mon module>.module.ts`
  - différents dossiers :
    - pages
    - components
    - services
    - directives
    - enums
    - interfaces
    - guards
    - etc ...

- situationnel :
  - le routing :
    - `<le nom de mon module>-routing.module.ts`
    - Lors de la création du module, rajouter à la fin de la commande `--routing` pour générer automatique le fichier de routing

>>>
[+ Bonne pratique +]
Mettre les routes relatives au module dans le fichier de routing du module. [- Ne pas tout mettre dans le app-routing.module.ts -].
>>>
### Les composants

- composition :
  - `<nom>.component.html`
  - `<nom>.component.scss`
  - `<nom>.component.spec.ts`
    - les tests unitairs de votre composant
  - `<nom>.component.ts`
    - @Component
      - selector --> la balise qui vous permet d'appeler ce composant dans l'HTML `<mon-composant></mon-composant>`
      - templateUrl --> le chemin à la page HTML de ce composant
      - styleUrls --> les chemins à vos stylesheet de ce composant

### Outils de visualisation

#### Compodoc

- Installation :
  - `npm i --save-dev @compodoc/compodoc`
  - `--save-dev` --> ajout dans les dépendances dev, ça n'impactera pas le build de prod

- Utilisation :
  - `compodoc -p tsconfig.json -o -s` --> à rajouter dans les commandes npm (dans package.json)
  - `-s` --> lancement en serveur
  - `tsconfig.json` --> à modifier si besoin

#### Webpack bundle analyzer

- Installation :
  - `npm i --save-dev webpack-bundle-analyzer`

- Utilisation :
  - `ng build --prod --stats-json && webpack-bundle-analyzer dist/crm/stats.json`
  - `ng build --prod --stats-json` --> build prod + création d'un fichier de stats
  - `webpack-bundle-analyzer dist/crm/stats.json` --> analyze du fichier de stats

## Les fondamentales

- déclaration des modules
  - chaques modules doivent être déclarer dans les imports de son module père
  - exemple : les modules core / shared / page-not-found / ui doivent être décarer dans app.module.ts dans les imports[]

- les modules utilisant le routing d'angular (la balise `<router-outlet></router-outlet>`) doivent importer `RouterModule`

- les composants shared
  - déclaration des componants dans les déclarations du module
  - déclaration des componants partager dans l'export de son module
  - exemple : appeler le composant ui depuis app module --> déclarer le composant `UiComponent` dans les exports pour l'appeler dans `app.component.html`

## Le routing

### Module routing

Les modules qui ont besoin de routing sont les modules avec des pages. Les modules obligatoires : ui / shared / core n'ont pas besoin d'être accessible par url. Alors que le module page-not-found, a besoin d'être accessible par url. il faut donc créer un fichier de routing ou s'implement lors de la création du module d'ajouter `--routing` après la commande de création.

>Pour la nomination des fichiers ce sera `<le-nom-de-votre-module>-routing.module.ts`

- Dans le fichier app-routing.module.ts vous avec :
  - une constante `routes`
    - c'est l'endroit où vous allez enseigner les routes de cette façon :
      - `{path:'monurl', component: monComponent}` --> le classique
      - `{path:'monurl', component: monComponent, canActivate:[monGuard1,monGuard2]}` --> avec des guards
      - `{path:'monurl', component: monComponent, children: [{ path: 'maSousUrl', component: monSousComposant}]}` --> avec des enfants (`/monurl/maSousUrl`)
      - [+ pour toutes les urls : utiliser le path:'**'+]
  - @NgModule :
    - imports :
      - `RouterModule.forRoot(routes)` dans le app-routing.module.ts
      - `RouterModule.forChild(routes)` dans les autres modules
    - exports
      - RouterModule --> pour que les routes soient accessibles

>>>
[+Bonne pratique des routes :+] 
Mettre les routes relative au module dans le fichier de routing du module en question !

exemple : les routes de page-not-found seront dans le fichier `page-not-found-routing.module.ts` et non dans app-routing.module.ts

[- Aucune route ne doit être importer dans app-routing.module.ts -], les routes sont automatiquements détectées car les modules sont importés dans app.module.ts puis les fichiers de routes sont importés dans les déclarations des modules.

Pour voir toutes les routes actives, copier coller dans app.module.ts :
```ts
const replacer = (key: string, value: any) => (typeof value === 'function') ? value.name : value;

console.log('Routes: ', JSON.stringify(router.config, replacer, 2));
```

Voir dans le app.module.ts, les routes sont affichées dans la console du navigateur par ordre de priorité.
[- Les routes ont une priorité en fonction de leur place dans -] `<le-nom-de-votre-module>-routing.module.ts` [- et dans les déclaration des modules dans app.module.ts-]
>>>

### Le lazyloading

Le lazyLoading est une configuration du fonctionnement du chargement d'Angular. C'est l'utilisation des modules et composants de façon optimisée. Dans votre application, si elle commence à grossir, certains modules ne seront pas utilisés par tous les utilisateurs, ou encore, avant d'acceder à certaines pages il faut impérativement passer par d'autres. Le but du lazyloading est d'optimiser le chargement d'angular pour qu'il charge les modules important en premier et ensuite, charger les modules complémentaires lors que la bande passante se libère (il y a d'autre cas mais celui là est le plus commun).

Angular de base fonctionne comme une OnePage et charge l'intégralité de votre application en cache dans votre navigateur est ensuite via le routing installé il vou affiche des pages.Le but ici est de découper l'application pour charger angular par petits morceaux.

Pour cela nous allons utilisé Compodoc pour visualiser si le lazyloading est bien effectué.

Voici l'application sans le lazy loading :

![alt text](src/assets/imgDoc/sansLazyloading.png "sans lazy loading")

et voici l'application avec le lazy loading :

![alt text](src/assets/imgDoc/avecLazyloading.png "sans lazy loading")

On peut remarquer que certain composant se retrouvent séparés du AppModule. Pour notre application, nous allons utiliser le lazy loding sur le module `Personne` et `PageNotFound`. Ce sont des Modules qui ne sont pas chargés dans tous les cas d'usage de l'application. 

Je vous invite à regarder la documentation dans `/documentation`, il suffit de lancer le index.html.

Pour implémenter le lazy loading il faut 5 étages :

1. Supprimer le modules que l'on veut lazy loader dans les imports du `app.module.ts`
2. Dans le app-routing.module.ts, rajouter la ligne pour indiquer qu'il faut charger quand même le module :
```ts
{path: 'personnes', loadChildren: () => import('./personne/personne.module').then((fichier) => fichier.PersonneModule)}
```
3. Dans le `<module>-routing.module.ts` supprimer le path par defaut : ici c'est /personnes, dans `personne-routing.module.ts` :
```ts
{path:'',component: ListPersonnesComponent,canActivate:[ConnexionGuard]},
{path:'add',component: AddPersonneComponent,canActivate:[ConnexionGuard]},
{path:':id',component: EditPersonneComponent,canActivate:[ConnexionGuard]}
```
4. Définir la stratégie de chargement des modules dans le app-routing.module.ts, dans les imports :
```ts
imports: [RouterModule.forRoot(routes,{
    preloadingStrategy:PreloadAllModules
  })],
  ...
```
5. Vérifier l'import du app-routing.module.ts pour les prioritées des routes

Il existe différentes stratégies de chargement d'angular :

- NoPreloading        --> ne rien pré charger, charger uniquement les modules rattaché au AppModule
- PreloadAllModules   --> charger tous les modules, mais en priorité les principaux, et une fois qu'on a de la bande passante : charger les modules complémentaires

PreloadAllModules est le plus souvent utilisé.
NoPreloading est utilisé souvent quand Angular est utilisé avec un autre front (très spécifique)

[+Implémenter le lazyloading dès le début du projet. Il sera peut-être plus possible de l'implémenter après certains développements ou du moins il faudra refactorer du code et donc une possibilité de régression.+]

## Les "à savoir"

### Le scss

- Utilisation de Bootstrap :
  - importer bootstrap dans le fichier styles.scss
  - ligne : `@import '~bootstrap/scss/bootstrap';`

- les stylesheets custom :
  - créer un fichier styling dans src
  - mettre dans /styling tous les fichiers de style
  - rajouter dans angular.json en dessous de `styles` :

```json
"stylePreprocessorOptions": {
  "includePaths": [
    "src/styling"
  ]
}
```

-
  - importer dans styles.scss les styles que vous voulez avec seulement :
    - `@import 'font';` pas besoin de mettre de chemin spécifique

### Les enums

>Les enums doivent ce situer dans /shared/enums, à voir pour votre projet ce qui doit être mis dans les enums pour une meilleure maintenabilité

Un enum est un object qui permet de stocker des valeurs et d'y acceder par des keys, c'est très pratique pour stocker des informations qui ne changent pas : les messages d'erreurs, les messages d'informations, etc ...

exemple d'enum :

```ts
export enum Message {
  MESSAGE_ERREUR_DONNEE = 'les données sont invalides',
  MESSAGE_CONFIRMATION_SAUVEGARDE = 'les données ont bien été sauvegardées',
}
```

Il suffit d'importer l'enum dans le typeScript : 
```ts
Message.MESSAGE_ERREUR_DONNEE
```

#### Les enums dans l'HTML

Il est possible d'utiliser les enums dans l'html mais il faut d'abors les déclarer dans le typeScript :
```ts
message = Message;
```

Ensuite il suffit d'appeler l'enum avec une extrapolation :
```html
{{message.MESSAGE_ERREUR_DONNEE}}
```

#### Les enums pour les selects

Et oui les enums peuvent être utilisés pour les définir les options des selects. En prenant pour exemple cette enum :

```ts
export enum MesOptions {
  OPTION1 = 'mon option 1',
  OPTION2 = 'mon option 2',
}
```

Il est possible de le transformer en tableau de valeur pour l'utiliser avec un ngfor dans l'HTML :

```ts
MesOptionsValues = Object.values(MesOptions);
```

Il suffit ensuite de boucler sur MesOptionsValues avec un ngfor sur la balise `<option>` du `<select>`

[+ Il est aussi possible de créer un tableau de keys avec : Object.keys(MesOptions) +]

### Les interfaces

Le typeScript est le petit frère du javaScript mais il est beaucoup mieux organisé : le typage des variables / constantes est de rigeur. Les interfaces permettent de facilité le typage des objets afin de consolider la relation Front / Back.

>Les interfaces doivent se situer dans /shared/interfaces

Le but des interfaces et de former une variable avec un paterne d'attribut. Voici un exemple :

```ts
export interface Client{
  name: string;
  email: string;
  comment:string;
  state:ClientState;
  id?:number;
}
```

Dans cette exemple on peut voir des attributs classique : name / email / comment.

Pour l'attribut state : il utilise un enum qui lui permet de prédéfinir les valeur qu'il peut prendre.

Pour l'attribut id : le ?: permet d'annoncer qu'il ne sera pas tout le temps renseigné.

### Dans l'html

Angular facilite le dynamisme des pages html avec des outils très simples d'utilisations.

#### Les évènements ()

- Dans les balises HTML angular rajoute des évènements qui permettent de faire appel à des fonctions du typeScript :
  - `(click)="mafonction()"` --> classique
  - `(submitted)="mafonction($event)"` --> avec un event en paramètre de fonction
  - `(retourDeComposant)="mafonction()"` --> un retour de composant (via @Output)
  - `(ngModelChange) / (change) / (onChange)` ---> sur n'importe quel élément intéractif

[- c'est une solution de facilité donc pas focément la bonne solution !! à bien utiliser dans les cas utiles -]. Par exmple : ne pas utiliser (change) à la place de ngForm.

Les () permettent d'appeler une fonction du typeScript depuis l'html via le déclanchement d'un évènement.

#### Les propriétés []

Les [] font l'inverses des () : ils permettent d'importer / d'utiliser des variables / constantes / enums / etc ... du typeScript dans l'HTML.

- Les cas les plus fréquent sont :
  - `[ngClass]="{'maClasse': monBoolean}"` --> ajoute une classe à une balise en fonction du boolean
  - `[monAttribuDeMonSousComponent]="ma variable"` --> permet de passer des valeurs au composant fils (via @Input)

#### NgModel \[\(\)\]

NgModel est à utiliser avec parcimonie : il peut être gourmand.

1. il faut importer `FormsModule` dans le module où vous allez utiliser NgModel

NgModel permet de synchroniser une variable du typescript avec un élément intéractif de l'html :

```html
<input type="text" [(ngModel)]="testNgModel">
```

```ts
testNgModel = '';
```

Vous pouvez afficher testNgModel dans l'HTML via une extrapolation simple : 
```html
{{testNgModel}}
```

#### Les extrapolations {{}}

simple utilisation des variables / constantes du typeScript dans l'HTML, à utiliser s'il n'y a pas besoin d'utiliser les [] !

- manipulation via les pipes | :
  - `{{testNgModel | UpperCasePipe}}` --> mettre en majuscule
  - [https://angular.io/api/common#pipes]

#### Ngif

Permet de tester une condition boolean et d'afficher où non l'élément et ses fils.

Exemple : 
```html
*ngIf="testNgModel.length > 10"
```

#### Ngfor

Permet de boucler sur une liste.

Exemple : 
```html
*ngFor="let letter of testNgModel.split('')"
```

#### Ng-container

permet d'utiliser une balise sans qu'elle ne s'affiche dans le DOM final :

- Utilisable avec des ngif ou des ngfor
- [+ à utiliser lors qu'une balise est utiliser uniquement pour un ngif ou ngfor +]

#### Ng-template

Permet de créer des template dans l'html qui peuvent êtres utilisés ensuite dans le type script ou directement dans l'HTML :
- via les ngIfElse
- via les ngModale dans le typeScript : [https://ng-bootstrap.github.io/#/components/modal/examples]

### Dans le type script

#### Les constructeurs

Une fois qu'un service est déclarer dans le constructeur, il n'y a pas besoin de le déclarer autre part :

exemple :

```ts
constructor(private route : ActivatedRoute) { }

mafonction(){
  this.route.paramMap.subscribe(
    (param) => {
      console.log(param)
    }
  )
}
```

#### Les fonctions

classique : (context créé par la fonction)

```ts
fonction(){}
```

flêché : (pas de nouveau context)

```ts
e => {}
ou
() => {}
ou
(e) => {}
```

paramètres infini :

```ts
function(...a){}
ou
function(e,...a){}
```

#### Copie d'objet

utilisation des `...` :

```ts
let monArray = ['test','test2']
let maNouvelleArray = monArray
maNouvelleArray.push('test3') --> modification de monArray et maNouvelleArray

mais

let monArray = ['test','test2']
let maNouvelleArray = [...monArray]
maNouvelleArray.push('test3') --> modification de maNouvelleArray uniquement
```

#### Utilisation des altgr+7

En utilisant ces quotes, vous pouvez faire des sauts de ligne dans le typeScript et intégrer des variable comme ceci :
```ts
`mon texte ${maVariable} mon texte`
```

## Relation componant componant

### Réutilisation mais pas trop de souplesse

Un composant est une boite que l'on peut construire de façon à lui passer les paramètres via des Input mais aussi recevoir des events via des Output. Le but est de faires des composants réutilisatables : formulaires, boutons, tableau, dropdown, etc ... ces composants réutilisables seront stocké dans le module `shared` afin qu'ils soient accessibles aux autres modules.

Seulement il ne faut pas tomber dans la boucle de créer des composants ultra souple avec pleins de paramètres, pleins d'output etc ... Créez des composants simple avec une utilité propre.

### Input

Les @Input() permettent de passer des informations au composant fils. Cela ce fait dans la balise d'appel au composant :
```html
<app-mon-composant monInput1="info1" [mmonInput2]="variable1"></app-mon-composant>
```

Dans le composant fils, lors de la déclaration des inputs, il est possible de définir une valeur par défaut pour que le composant ait une fonction de base. Par exemple pour un formulaire, si il on ne lui passe pas d'information, le fomulaire aura des champs vide et retournera les valeurs une fois le formulaire envoyé.
```ts
@Input()
personne:Personne = {
  nom:'',
  prenom:'',
  age:'',
}
```

De cette façon, le formulaire Personne peut-être utilisé pour créer une nouvelle personne. Dans le cas où l'on passe des informations, les champs seront alors prés remplis et le formulaire aura comme fonction de modifier les informations.

[+ exmple add-personne et edit-personne +]

### Output

Pour les outputs, donc passer des informations du fils au parent, cela ce fait via les eventEmitters (de `@angular/core`) :

```ts
@Output()
eventSubmitter = new EventEmitter;

register(){
  this.eventSubmitter.emit(this.formPersonne.value)
}
```

Dans notre cas, on parlera du formulaire pour les personnes : ici on déclare un EventEmitter que l'on va déclancher quand on cliquera sur valider le formulaire. on va emmettre depuis le composant fils pour que le composant parent puisse déclancher un event propre à l'utilité du parent :
```html
<app-form-personne ... (eventSubmitter)="maFonction($event)"></app-form-personne>
```

En effet le formulaire ici dans ce projet est utiliser pour deux choses : créer une personne ou modifier une personne. Les deux composants vont appeler le composant formulaire et les deux feront des actions différentes mais dans leur composant.

[-Il n'est pas question de traiter les actions des composants parents dans le composant fils.-]

### Content

Les composants qui peuvent être appelés plusieurs fois et pour des tâches différentes doivent rester clean et ne proposer que le stict minimum. Par exemple le composant tableau, il ne doit proposer que le strict minimum : s'occuper des headers. Pour le contenu, c'est au composant parent de s'occuper de lui fournir le bloc déjà construit !

Dans le composant tableau :

```html
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col" *ngFor="let header of headers">{{header}}</th>
    </tr>
  </thead>
  <tbody>
    <ng-content></ng-content>
  </tbody>
</table>
```

Nous utilisons la balise `<ng-content></ng-content>` : elle permet de récupérer le contenu de l'appel de parent qui ressemble à ceci :

```html
<app-tableau [headers]="headers">
  <tr class="table-active" *ngFor="let personne of listPersonnes">
    <td class="text-uppercase">{{personne.fields.nom}}</td>
    <td class="text-capitalize">{{personne.fields.prenom}}</td>
    <td>{{personne.fields.age}}</td>
    <td><app-dropdown [title]="titleDropdown" [options]="optionsDropdown" (clickEmitter)="choixDropdown($event,personne)"></app-dropdown></td>
  </tr>
</app-tableau>
```

Le contenu que le parent passe à son fils est compris dans les balises d'appels : `<app-tableau ...></app-tableau>`. Il est possible de passer plusieur blocs de contenue via des #id définit dans le composant parent sur les balises mais cela reste très spécifique.

## Les formulaires

Pour les formulairs, il existe deux types de façons de faires. Les deux ont leur utilité propre:

- via l'HTML : en utilisant NgModel
- via le ts : en utilisant FormBuilder

### NgModel

NgModel a été présenté un peut plus haut, il est simple de récupérer des informations de l'html de façon ultra rapide. C'est très pratique pour de petites prisent d'informations comme un login ou un mot de passe ou encore de la modification à la volée.

Importer `FormsModule` dans le module où est utilisé NgModel

--> exemple sur la page 404 du projet

### FormBuilder

Pour la création du formulaire avec formBuilder, c'est plus fastidieux. 

En premier lieu, importer `ReactiveFormsModule` dans le module où sera utiliser FormBuilder.

Il faut ensuite créer un formulaire côté ts :

```ts
formPersonne!:FormGroup
...
this.formPersonne = this.formBuilder.group({
  test:[],                                                                      --> classique
  age: ['valeur de base'],                                                      --> avec une valeur de base
  nom: ['valeur de base',Validators.required],                                  --> avec un Validators
  prenom: ['valeur de base',[Validators.required,Validators.maxLength(10)]],    --> avec plusieurs validators
})
```

Une fois le fomulaire créé côté ts, il faut le lier au formulaire de l'HTML :

```html
<form ... [formGroup]="formPersonne">
```

Via `formGroup`, on indique au fomulaire qu'il aura le template prés indiqué dans le ts.

```html
<input formControlName="nom" ...>
```

Pour les champs du formulaire, on va lier les attributs du formGroup via leur id.

```html
<form (ngSubmit)="register()" ...>
  ...
  <button type="submit" ... >...</button>
  ...
</form>
```

Pour finir, il faut un bouton du type submit et l'évènement ngSubmit qui ce déclanchera quand on cliquera sur le bouton.

#### Les validators

La partie précédente est fastidieuse mais permet de mettre en place tout un système de validation des champs. Avec les validateurs que nous avons mis :

- Validators.required

Il suffit de remplir le champs pour le formulaire soit valide, autrement le formulaire ne le sera pas. Pour tester si le fomulaire est valide, il suffit d'utiliser :

```ts
formPersonne.valid --> varie en foction de la validité du formulaire
```

Grâce à cela, il est simple d'empêcher l'utilisateur d'envoyer le formulaire, par exemple en rendant inatif le bouton d'envoi :

```html
<button ... [disabled]="!formPersonne.valid">...</button>
```

Mais il est aussi possible de tester la validité de chaques champs :

1. récupérer le FormControl du champs qui m'intéresse

```ts
get nom(){
  return this.formPersonne.get('nom') as FormControl
}
```

2. tester la validité du champs

```html
<input formControlName="nom" ... [ngClass]="{'is-valid': nom.valid}">
```

Dans ce cas, lorsque le champs `nom` est valide, la class is-valid sera rajouté au champs (class bootstrap).

## Les services

### RXJS Subject

`Reactive Extensions Library for JavaScript` est une librairie composée de composants asynchrone basé sur des évênements et des observables. Lien pour pour le RXJS : [https://rxjs.dev/guide/overview](rxjsOverview)

Pour commencer, RXJS met à disposition des `subject`, chaque subjects ont leur propre mode de fonctionnement : [https://rxjs.dev/guide/subject](les différents subjects)

Voici une liste des différents subjects

- Subject
- BehaviorSubject
- ReplaySubject
- AsyncSubject

Le plus souvent utilisé sera le BehavioSubject qui est un subject qui garde en mémoire un état qu'on lui donne. Par exemple, pour une connexion le behaviorSubject va garder en mémoir un boolean pour savoir si on est connecté ou pas. Lors qu'une personne se connecte, on va lui passer un nouveau boolean qui sera son nouvelle état. Il ne garde pas en mémoir ses anciens états.

Il est possible de modifier son état via `next()`:

```ts
connexion = false;
connexionB = new BehaviorSubject(this.connexion);

myfonction(c:boolean){
  this.connexionB.next(c)
}
```

A chaque modifications de l'état du BehaviorSubject, il emmet un êvénement qui va provoquer la mise à jour des souscriptions qui sont faites sur le BehaviorSubject.

```ts
this.monService.connexionB.subscribe(
  (data)=>{
    console.log(data)
  }
)
```

Dans cette exemple, à chaque fois que l'état du behaviorSubject va changer, un console.log sera affiché.

### RXJS Observable

Les Observables sont des Subjects qui sont utilisés pour observer des tâches asynchrones. Les requêtes http retournent des observables qui mettent à jours la souscription dès qu'ils recoivent des changements sur l'objet qu'ils observent.

Lien du site rxjs : [https://rxjs.dev/guide/observable](utilisation des observables)

Il existe différents pipe pour utiliser les observables :

- les pipes
  - distinctUntilChanged()
  - take(x)
  - forkJoin

Dans notre cas, nous allons utiliser les Observables pour observer les BehaviorSubject. Il est vrai qu'il est possible de souscrire à un BehaviorSubject directement. Seulement les Observables ont un atout qui est d'être en lecture seul. Ce qui permet de souscire à un BehaviorSubject sans pouvoir le modifier et de créer de la sécurité afin de localiser les changements dans un service.

[+Bonne pratique :+]
```ts
private connexion = false;
private connexionB = new BehaviorSubject(this.connexion);
connexionO = this.connexionB.asObservable()

setConnexion(c:boolean){
  this.connexion = c
  this.connexionB.next(this.connexion)
}
```

Ainsi on sait que la connexion ne sera seulement modifier en utilisant la fonction `setConnexion` et nul part ailleurs.

### Les HOT / COLD subscribe

[-Danger :-] les souscriptions à des Subjects sont susceptible de créer des fuites de mémoire. Il va vous falloir déterminer pour chaques souscriptions si elle est hot ou cold.

- les hot :
  - ce sont les souscriptions qui ne se terminent jamais
- les cold :
  - ce sont les souscriptions qui se terminent après un processus

Vous devez vous demander si votre souscription sera détruite automatiquement ou pas. 

Exemple n°1 : vous souscrivez à une Promise (ou à une requêtes http). Cette souscription est une souscription cold parce qu'après que le Promise est été `reject` ou `resolve` (OU que votre requête http ait reçu une réponse négtive ou positive), la souscription sera détruite.

Exemple n°2 : vous souscrivez à un BehaviorSubject. Cette souscription ne se termine jamais parce que le BehaviorSubject n'a pas de fin, il existera toujours et il emmettra dès qu'il aura un nouvelle état. Dans ce cas, la souscription est une souscription hot. Il faut dans ce cas détruire manuellement la souscription :

1. implémenter `OnDestroy` :

```ts
export class LoginComponent implements OnInit,OnDestroy{
  ...
  ngOnDestroy(): void {}
  ...
}
```

2. créer une variable global pour accéder à la souscription :

```ts
subConnexion!:Subscription

ngOnInit(): void {
  this.subConnexion = this.connexionService.connexionO.subscribe( ... )
}
```

3. supprimer la souscription à la destruction du composant :

```ts
ngOnDestroy(): void {
  this.subConnexion.unsubscribe()
}
```

[-à faire obligatoirement pour les hot suscriptions, sinon une nouvelle souscription en plus de l'ancienne sera créé dès que le composant sera reappelé-]

### Les guards

Les guards sont des vérifications qui sont applicable sur les URLs de votre application. ils permettent de dire si oui ou non une url est accessible. Par exemple si votre application a un système de connexion, il est possible de rendre accessible des urls si l'utilisateur est connecté ou non, en fonction de son rôle, etc ...

[+Les guards doivent êtres créer dans /core/guards, se sont des utilitaires pour toute l'application.+]

Pour créer un guard, il vous suffit d'utiliser la cli d'angular : `ng g guard <name>`

Les guards ont une fonction principal : `canActivate` qui retourne au choix :

- Observable<boolean | UrlTree> 
- Promise<boolean | UrlTree>
- boolean
- UrlTree

Pour faire simple, au retours de cette fonction :

- true --> url accessible
- false --> url bloquée

Dans notre exemple nous allons lier la connexion avec les Observables de RXJS. On vois qu'on peut retourner un Observable pour savoir si une url peut être accessible ou pas, alors nous allons utiliser l'Observable de la connexion afin de savoir si les urls gardé par ce guard sont accessible :

```ts
constructor(private connexionService:ConnexionService){}

canActivate(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  return this.connexionService.connexionO;
}
```

Tout simple, dans le constructeur, on appel le service connexion et ensuite il suffit simplement de retourner l'Observable qui regarde le BehaviorSubject qui contient uniquement des états boolean.

Ensuite il suffit de dire lors de la déclaration de l'url si elle doit être gardé par un guard :

```ts
{path:'personnes',component: ListPersonnesComponent,canActivate:[ConnexionGuard]}
```

Ainsi l'url /personnes ne sera accessible uniquement si on est connecté.
