import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'personnes', loadChildren: () => import('./personne/personne.module').then((fichier) => fichier.PersonneModule)},
  {path: '**',loadChildren: () => import('./page-not-found/page-not-found.module').then((fichier) => fichier.PageNotFoundModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    preloadingStrategy:PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
