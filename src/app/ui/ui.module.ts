import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiComponent } from './container/ui/ui.component';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';



@NgModule({
  declarations: [
    UiComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    UiComponent
  ]
})
export class UiModule { }
