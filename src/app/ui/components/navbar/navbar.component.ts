import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ConnexionService } from 'src/app/login/services/connexion.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  connexion!:Observable<boolean>


  constructor(private connexionService:ConnexionService) { }


  ngOnInit(): void {
    this.connexion = this.connexionService.connexionO
  }



}
