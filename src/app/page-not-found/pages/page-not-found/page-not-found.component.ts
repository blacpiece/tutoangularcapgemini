import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Faces } from 'src/app/shared/enums/faces';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  testNgModel = 'coucou';
  faces = Object.values(Faces);

  constructor(private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (param) => {
        console.log(param)
      }
    )
  }

  clickButton(template:any){
    console.log(template)
  }

}
