import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TableauComponent } from './components/tableau/tableau.component';



@NgModule({
  declarations: [
    ButtonComponent,
    DropdownComponent,
    TableauComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    ButtonComponent, DropdownComponent,TableauComponent
  ]
})
export class SharedModule { }
