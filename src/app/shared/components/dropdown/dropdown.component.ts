import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {

  @Input()
  title = 'myDropdownTitle'

  @Input()
  options = ['option1','option2']

  @Output()
  clickEmitter = new EventEmitter();

  clickOption(option:string){
    this.clickEmitter.emit(option);
  }

}
