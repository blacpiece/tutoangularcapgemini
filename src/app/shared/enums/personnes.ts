export enum OptionsListPersonnesDropdown {
  EDIT = 'éditer',
  DELETE = 'supprimer',
}

export enum PersonnesConstantes {
  TITLE_DROPDOWN_LIST_PERSONNE = 'Options'
}

export enum PersonnesHeaderTableau {
  NOM = 'Nom',
  PRENOM = 'Prénom',
  AGE = 'Age',
  OPTIONS = 'Options'
}
