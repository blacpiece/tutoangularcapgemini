import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConnexionService {

  private connexion = false;
  private connexionB = new BehaviorSubject(this.connexion);
  connexionO = this.connexionB.asObservable()

  CONNEXION = 'connexion'
  TRUE = 'true'

  constructor() {
    if (localStorage.getItem(this.CONNEXION)) {
      this.connexion = localStorage.getItem(this.CONNEXION) == this.TRUE
      this.connexionB.next(this.connexion)
    } else {
      localStorage.setItem(this.CONNEXION,''+this.connexion)
    }
  }

  setConnexion(c:boolean){
    this.connexion = c
    localStorage.setItem(this.CONNEXION,''+this.connexion)
    this.connexionB.next(this.connexion)
  }
}
