import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ConnexionService } from '../../services/connexion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit,OnDestroy{

  login!:string
  password!:string
  connexionEtat!:boolean
  subConnexion!:Subscription

  constructor(private connexionService:ConnexionService){}

  ngOnInit(): void {
    this.subConnexion = this.connexionService.connexionO.subscribe(
      (connexion) => {
        this.connexionEtat = connexion
        console.log('actu connexion')
      }
    )
  }

  ngOnDestroy(): void {
    this.subConnexion.unsubscribe()
  }

  connexion(){
    console.log(this.login+' '+this.password)
    this.connexionService.setConnexion(true)
  }

  deconnexion(){
    this.connexionService.setConnexion(false)
  }

}
