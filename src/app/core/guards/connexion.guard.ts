import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ConnexionService } from 'src/app/login/services/connexion.service';

@Injectable({
  providedIn: 'root'
})
export class ConnexionGuard implements CanActivate {


  constructor(private connexionService:ConnexionService){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.connexionService.connexionO;
  }

}
