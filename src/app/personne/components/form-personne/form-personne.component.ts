import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Personne } from 'src/app/shared/interfaces/personne';

@Component({
  selector: 'app-form-personne',
  templateUrl: './form-personne.component.html',
  styleUrls: ['./form-personne.component.scss']
})
export class FormPersonneComponent implements OnInit {

  @Output()
  eventSubmitter = new EventEmitter;

  formPersonne!:FormGroup

  @Input()
  personne:Personne = {
    nom:'',
    prenom:'',
    age:'',
  }

  @Input()
  typeForm = 'ajouter'

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formPersonne = this.formBuilder.group({
      nom: [this.personne.nom,Validators.required],
      prenom: [this.personne.prenom,Validators.required],
      age: [this.personne.age]
    })
  }

  get nom(){
    return this.formPersonne.get('nom') as FormControl
  }

  get prenom(){
    return this.formPersonne.get('prenom') as FormControl
  }

  register(){
    this.eventSubmitter.emit(this.formPersonne.value)
  }

}
