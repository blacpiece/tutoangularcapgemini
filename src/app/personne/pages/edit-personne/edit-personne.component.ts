import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Personne } from 'src/app/shared/interfaces/personne';
import { PersonnesService } from '../../services/personnes.service';

@Component({
  selector: 'app-edit-personne',
  templateUrl: './edit-personne.component.html',
  styleUrls: ['./edit-personne.component.scss']
})
export class EditPersonneComponent implements OnInit {

  personne!:Personne
  id!:string

  constructor(private route:ActivatedRoute,private personnesService: PersonnesService,private router:Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (param) => {
        this.id = param.get('id') as string
        if(this.id){
          this.personnesService.getPersonne(this.id).subscribe(
            (data:any) => {
              this.personne = data.fields
              console.log(this.personne)
            },
            (error) => {
              console.log(error)
            }
          )
        }
      }
    )
  }

  updatePersonne(personne:Personne){
    this.personnesService.updatePersonne(this.id,personne).subscribe(
      (data) => {
        console.log(data)
        this.router.navigate(['personnes'])
      },
      (error) => {
        console.log(error)
      }
    )
  }

}
