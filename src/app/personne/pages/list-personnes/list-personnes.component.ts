import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/shared/interfaces/personne';
import { PersonnesService } from '../../services/personnes.service';
import { map, } from 'rxjs/operators';
import { OptionsListPersonnesDropdown, PersonnesConstantes, PersonnesHeaderTableau } from 'src/app/shared/enums/personnes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-personnes',
  templateUrl: './list-personnes.component.html',
  styleUrls: ['./list-personnes.component.scss']
})
export class ListPersonnesComponent implements OnInit {

  listPersonnes!:any;

  optionsDropdown = Object.values(OptionsListPersonnesDropdown);
  titleDropdown = PersonnesConstantes.TITLE_DROPDOWN_LIST_PERSONNE;
  headers = Object.values(PersonnesHeaderTableau)

  constructor(private personneService:PersonnesService,private router:Router) { }

  ngOnInit(): void {
    this.personneService.list().subscribe(
      (data:any)=>{
        this.listPersonnes = data.records
      },
      (error)=>{
        console.log(error)
      }
    )
  }

  choixDropdown(event:any,personne:any){
    switch(event){
      case OptionsListPersonnesDropdown.DELETE:
        this.personneService.deletePersonne(personne.id).subscribe(
          (data)=>{
            console.log(data)
            this.ngOnInit()
          },
          (error)=>{
            console.log(error)
          }
        )
        break;
      case OptionsListPersonnesDropdown.EDIT:
        this.router.navigate(['/personnes/'+personne.id])
        break;
    }
  }

}
