import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Personne } from 'src/app/shared/interfaces/personne';
import { PersonnesService } from '../../services/personnes.service';

@Component({
  selector: 'app-add-personne',
  templateUrl: './add-personne.component.html',
  styleUrls: ['./add-personne.component.scss']
})
export class AddPersonneComponent implements OnInit {

  constructor(private personnesService : PersonnesService,private route:Router) { }

  ngOnInit(): void {
  }

  addPersonne(personne:Personne){
    console.log(personne)
    this.personnesService.addPersonne(personne).subscribe(
      (data)=>{
        console.log(data)
        this.route.navigate(['personnes'])
      },
      (error)=>{
        console.log(error)
      }
    )
  }

}
