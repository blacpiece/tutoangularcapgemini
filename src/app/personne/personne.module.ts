import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonneRoutingModule } from './personne-routing.module';
import { ListPersonnesComponent } from './pages/list-personnes/list-personnes.component';
import { AddPersonneComponent } from './pages/add-personne/add-personne.component';
import { FormPersonneComponent } from './components/form-personne/form-personne.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { EditPersonneComponent } from './pages/edit-personne/edit-personne.component';



@NgModule({
  declarations: [
    ListPersonnesComponent,
    AddPersonneComponent,
    FormPersonneComponent,
    EditPersonneComponent
  ],
  imports: [
    CommonModule,
    PersonneRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class PersonneModule { }
