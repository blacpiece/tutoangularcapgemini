import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Personne } from 'src/app/shared/interfaces/personne';

@Injectable({
  providedIn: 'root'
})
export class PersonnesService {

  apiKey = 'keyNEPjIBVeEznmqm';

  constructor(private http:HttpClient) { }

  list(){
    const headers = new HttpHeaders().set("Authorization", "Bearer "+this.apiKey);
    return this.http.get('https://api.airtable.com/v0/appSj5OMd3RjcDodi/personnes',{headers})
  }

  getPersonne(id:string){
    const headers = new HttpHeaders().set("Authorization", "Bearer "+this.apiKey).set("Content-Type","application/json");
    return this.http.get('https://api.airtable.com/v0/appSj5OMd3RjcDodi/personnes/'+id,{headers})
  }

  addPersonne(personne:Personne){
    const headers = new HttpHeaders().set("Authorization", "Bearer "+this.apiKey).set("Content-Type","application/json");
    const body = `{"records": [{"fields": ${JSON.stringify(personne)}}]}`
    return this.http.post('https://api.airtable.com/v0/appSj5OMd3RjcDodi/personnes',body,{headers})
  }

  updatePersonne(id:string,personne:Personne){
    const headers = new HttpHeaders().set("Authorization", "Bearer "+this.apiKey).set("Content-Type","application/json");
    const body = `{"records": [{"id": "${id}","fields": ${JSON.stringify(personne)}}]}`
    return this.http.patch('https://api.airtable.com/v0/appSj5OMd3RjcDodi/personnes',body,{headers})
  }

  deletePersonne(id:string){
    const headers = new HttpHeaders().set("Authorization", "Bearer "+this.apiKey);
    return this.http.delete('https://api.airtable.com/v0/appSj5OMd3RjcDodi/personnes/'+id,{headers})
  }


}
