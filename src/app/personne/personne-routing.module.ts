import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionGuard } from '../core/guards/connexion.guard';
import { AddPersonneComponent } from './pages/add-personne/add-personne.component';
import { EditPersonneComponent } from './pages/edit-personne/edit-personne.component';
import { ListPersonnesComponent } from './pages/list-personnes/list-personnes.component';

const routes: Routes = [
  {path:'',component: ListPersonnesComponent,canActivate:[ConnexionGuard]},
  {path:'add',component: AddPersonneComponent,canActivate:[ConnexionGuard]},
  {path:':id',component: EditPersonneComponent,canActivate:[ConnexionGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonneRoutingModule { }
